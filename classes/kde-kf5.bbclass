inherit kde-base

KF5_VERSION = "5.65.0"

SRC_URI = "${KDE_MIRROR}/stable/frameworks/5.65/${BPN}-${PV}.tar.xz"
