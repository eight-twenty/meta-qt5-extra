SUMMARY = "Advanced internationalization framework"
LICENSE = "BSD & LGPLv2.1"
LIC_FILES_CHKSUM = " \
	file://COPYING-CMAKE-SCRIPTS;md5=3775480a712fc46a69647678acb234cb \
	file://COPYING.LIB;md5=2d5025d4aa3495befef8f17206a5b0a1 \
"

inherit kde-kf5

PV = "${KF5_VERSION}"
SRC_URI[md5sum] = "89e847e45c19baca140ae68f8f7b7a14"
SRC_URI[sha256sum] = "303c3ef4fc7e417233211373a21759332f39a9ae45c9ce7b407eaba412d2caa4"
