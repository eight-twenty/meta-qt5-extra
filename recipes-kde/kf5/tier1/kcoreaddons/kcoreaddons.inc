SUMMARY = "Addons to QtCore"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = " \
	file://COPYING.LIB;md5=2d5025d4aa3495befef8f17206a5b0a1 \
"

inherit kde-kf5 mime

PV = "${KF5_VERSION}"
SRC_URI[md5sum] = "f1dbb43eccbf7ad87dbb2f7a97563e67"
SRC_URI[sha256sum] = "0f334b123b307829d11a39f639845f2f74d290490b264d9b188f1a785e16bf41"
