SUMMARY = "Open Collaboration Services API"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = " \
	file://COPYING;md5=be254b9345b1c2ff33e1a6a96768f2fb \
"

inherit kde-kf5

SRC_URI[md5sum] = "7e70910ba0d294c7b4f9ca12d27e00c5"
SRC_URI[sha256sum] = "a62908517f9cf44fd13c2cb37868f6484710284bc85bd85b532c3b8b7fc2b9ea"

PV = "${KF5_VERSION}"
