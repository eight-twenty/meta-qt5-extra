SUMMARY = "High-level multithreading framework"
LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING.LIB;md5=2d5025d4aa3495befef8f17206a5b0a1"

inherit kde-kf5

PV = "${KF5_VERSION}"
SRC_URI[md5sum] = "129fb82e1623b01ff1b9f8e1afb31358"
SRC_URI[sha256sum] = "19d74c5feb15903047d8bdf7fd1c94b4b6d0d22f3c860ff99ed1ef00a1f4b8b0"
