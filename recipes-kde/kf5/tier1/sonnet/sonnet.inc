SUMMARY = "Support for spellchecking"
LICENSE = "GPLv2 | LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING.LIB;md5=2d5025d4aa3495befef8f17206a5b0a1"

inherit kde-kf5

PV = "${KF5_VERSION}"
SRC_URI[md5sum] = "cc6d393858d809902be1b872a0e572c8"
SRC_URI[sha256sum] = "1f63ccfa8266e167fd996a253a3d1933a913c12e829056c74fe335fbfb327cbc"
